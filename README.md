#Versão JDK
    -JDK8
# guia-back-end

Destinado aos desenvolvedores backend

## Instrução para iniciar o backend
### Prerequisitos principais (Windows e Linux)
   - IDE de desenvolvimento - IntelliJ Community link abaixo
     https://www.jetbrains.com/idea/download/#section=linux
   - Maven para gerenciamento de pacote  do .war ou .jar
   - PostgresSQL Instalado
   - DBever, gerenciador de banco de dados
        - link para downloads https://dbeaver.io/download/
## Postgresql Docker 
#### Baixar a imagen do DockerHub
       docker pull postgres
##### Docker Linux `RUN`
       docker run -d \
            --name postgresql \
            -p 5432:5432 \
            -e POSTGRES_USERNAME=admin \
            -e POSTGRES_PASSWORD=Postgres2019! \
            -e POSTGRES_DBNAME=guiadb \
            -v /home/home/PostgreSQL:/var/lib/postgresql/data \
            frodenas/postgresql 
##### Docker windows `RUN`
      docker run -d\
       --name postgresql -p 5432:5432\
        -e POSTGRES_USERNAME=admin\
        -e POSTGRES_PASSWORD=Postgres2020!\
        -e POSTGRES_DBNAME=votingdb\
        -v postgres-data-volume:/var/lib/postgresql/data\
         frodenas/postgresql 

        
   
### Arquitetura
     MVC
### FRAMEWORKS ULTILIZADOS
    -   Spring / Spring Boot
    -   JPA/Hibernate
     
### Passos para iniciar localmente
    
   - fazer o clone do projeto; 
       - https://gitlab.com/bergSilva/sessionapi.git
   - Carregar no intelliJ;
   - Iniciar o projeto com o comando;
       - mvn clean spring-boot:run
   - Acessar o swagger no link 
      - http://localhost:8080/swagger-ui.html#

### API Disponivel na plataforma HEROKU NO LINK ABAIXO
     https://sesseionapi.herokuapp.com/swagger-ui.html

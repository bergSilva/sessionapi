package com.sessiomanager;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;


@SpringBootApplication
@EnableFeignClients
public class SessiomanagerApplication {

	public static void main(String[] args) {
		SpringApplication.run(SessiomanagerApplication.class, args);
	}

}

package com.sessiomanager.repository;

import com.sessiomanager.model.Pauta;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PautaRepository extends JpaRepository<Pauta, Integer> {
}

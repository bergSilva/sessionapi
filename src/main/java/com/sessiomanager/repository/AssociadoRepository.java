package com.sessiomanager.repository;

import com.sessiomanager.model.Associado;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AssociadoRepository extends JpaRepository<Associado, Integer> {
}

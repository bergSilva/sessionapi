package com.sessiomanager.dto;

import lombok.*;

import java.util.List;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class SessaoDto {
    private Integer id;
    private String tempo;
    private PautaDto pauta;
    private String dataFinalVotacao;
}

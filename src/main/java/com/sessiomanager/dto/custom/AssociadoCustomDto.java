package com.sessiomanager.dto.custom;

import lombok.*;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class AssociadoCustomDto {
    private Integer id;
    private String cpf;
}

package com.sessiomanager.dto.custom;

import lombok.*;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class SessaoCustomDto {
    private Integer id;
    private String time;
}

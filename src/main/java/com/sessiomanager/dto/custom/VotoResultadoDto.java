package com.sessiomanager.dto.custom;

import lombok.*;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class VotoResultadoDto {

    String tituloPauta;

    String sessao;

    private Integer totalVotosSim;

    private Integer totalVotosNao;

    private Integer totalGeralDeVotos;
}

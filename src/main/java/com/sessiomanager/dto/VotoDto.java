package com.sessiomanager.dto;

import com.sessiomanager.dto.custom.AssociadoCustomDto;
import com.sessiomanager.dto.custom.SessaoCustomDto;
import com.sessiomanager.model.Associado;
import com.sessiomanager.model.Sessao;
import lombok.*;

import javax.persistence.*;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class VotoDto {

    private Integer id;

    private AssociadoCustomDto associado;

    private SessaoCustomDto sessao;

    private String voto;
}

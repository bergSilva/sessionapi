package com.sessiomanager.dto;

import lombok.*;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class AssociadoDto implements Serializable {
    private Integer id;
    @NotBlank(message = "{cpf.not.blank}")
    private String cpf;
    private String nome;
}

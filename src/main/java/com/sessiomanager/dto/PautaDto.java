package com.sessiomanager.dto;

import lombok.*;
import org.springframework.data.annotation.Id;

import java.util.List;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class PautaDto {
    private Integer id;
    private String titulo;

}

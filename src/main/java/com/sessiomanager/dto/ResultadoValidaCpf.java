package com.sessiomanager.dto;

import lombok.*;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ResultadoValidaCpf {
    private String status;
}
package com.sessiomanager.exceptions;

public class DefaultException extends  RuntimeException{
    public DefaultException(String message) {
        super(message);
    }
}

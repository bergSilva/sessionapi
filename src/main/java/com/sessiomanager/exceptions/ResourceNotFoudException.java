package com.sessiomanager.exceptions;

public class ResourceNotFoudException extends  RuntimeException{
    public ResourceNotFoudException(String message) {
        super(message);
    }
}

package com.sessiomanager.exceptions;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

import java.util.List;

@Builder
@Getter
@AllArgsConstructor
public class ResourceExceptionsDetails {
    private String title;
    private  int status;
    private String details;
    private Long timestamp;
    private String developerMessage;
    private List<ObjectError> errorList;

    private ResourceExceptionsDetails() {
    }
}

package com.sessiomanager.handler;

import com.sessiomanager.exceptions.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@RestControllerAdvice
public class RestExceptionHandler {

    @ExceptionHandler(MethodArgumentNotValidException.class)
    protected ResponseEntity<?> handleMethodArgumentNotValid(MethodArgumentNotValidException ex) {
        List<ObjectError> errors = getErrors(ex);
        ResourceExceptionsDetails errorResponse = getErrorResponse(ex, HttpStatus.BAD_REQUEST, errors);
        return new ResponseEntity<>(errorResponse, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(ResourceNotFoudException.class)
    public ResponseEntity<?> handlerResourceNotFoundException(ResourceNotFoudException exception) {
        ResourceExceptionsDetails re = ResourceExceptionsDetails.builder()
                .timestamp(new Date().getTime())
                .status(HttpStatus.NOT_FOUND.value())
                .title("Resource Not Found")
                .details(exception.getMessage())
                .developerMessage(exception.getClass().getName())
                .build();

        return new ResponseEntity<>(re, HttpStatus.NOT_FOUND);

    }

    @ExceptionHandler(ConstraintViolationException.class)
    public ResponseEntity<?> handlerConstraintViolationException(ConstraintViolationException ex) {
        ResourceExceptionsDetails re = ResourceExceptionsDetails.builder()
                .timestamp(new Date().getTime())
                .status(HttpStatus.NOT_FOUND.value())
                .title("Constraint Violation")
                .details(ex.getMessage())
                .developerMessage(ex.getClass().getName())
                .build();

        return new ResponseEntity<>(re, HttpStatus.BAD_REQUEST);
    }
    @ExceptionHandler(DefaultException.class)
    public ResponseEntity<?> handlerDefaultException(DefaultException ex) {
        ResourceExceptionsDetails re = ResourceExceptionsDetails.builder()
                .timestamp(new Date().getTime())
                .status(HttpStatus.NOT_FOUND.value())
                .title("CPF Nao Habilitado")
                .details(ex.getMessage())
                .developerMessage(ex.getClass().getName())
                .build();

        return new ResponseEntity<>(re, HttpStatus.BAD_REQUEST);
    }


    /**
     * Exceção para parametros do DTO
     *
     * @param ex
     * @param status
     * @param errors
     * @return
     */

    private ResourceExceptionsDetails getErrorResponse(MethodArgumentNotValidException ex, HttpStatus status, List<ObjectError> errors) {

        ResourceExceptionsDetails re = ResourceExceptionsDetails.builder()
                .timestamp(new Date().getTime())
                .status(status.BAD_REQUEST.value())
                .title("Argumentos invalidos  no objeto")
                .details(ex.getMessage())
                .developerMessage(ex.getClass().getName())
                .errorList(errors)
                .build();

        return re;
    }

    private List<ObjectError> getErrors(MethodArgumentNotValidException ex) {
        return ex.getBindingResult().getFieldErrors().stream()
                .map(error -> new ObjectError(error.getDefaultMessage(), error.getField(), error.getRejectedValue()))
                .collect(Collectors.toList());
    }

}

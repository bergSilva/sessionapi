package com.sessiomanager.mapper;

import com.sessiomanager.dto.VotoDto;
import com.sessiomanager.dto.custom.AssociadoCustomDto;
import com.sessiomanager.model.Associado;
import com.sessiomanager.model.Sessao;
import com.sessiomanager.model.Voto;

public class VotoMapper {
    public Voto toDocument(VotoDto dto) {
        return Voto.builder()
                .id(dto.getId())
                .associado(Associado.builder().id(dto.getAssociado().getId()).build())
                .voto(dto.getVoto())
                .sessao(Sessao.builder().id(dto.getSessao().getId()).build())
                .build();
    }


    public VotoDto toDocumentDto(Voto entity) {
        return  VotoDto.builder()
                .id(entity.getId())
                .associado(AssociadoCustomDto.builder()
                        .id(entity.getId())
                        .cpf(entity.getAssociado().getCpf())
                        .build())
                .build();
    }

}

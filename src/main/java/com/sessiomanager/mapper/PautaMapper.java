package com.sessiomanager.mapper;

import com.sessiomanager.dto.PautaDto;
import com.sessiomanager.model.Pauta;

public class PautaMapper {

    public Pauta toEntity(PautaDto dto){
        return Pauta.builder()
                .id(dto.getId())
                .titulo(dto.getTitulo())
                .build();
    }

    public PautaDto toDto(Pauta entity){
        return PautaDto.builder()
                .id(entity.getId())
                .titulo(entity.getTitulo())
                .build();
    }

}

package com.sessiomanager.mapper;


import com.sessiomanager.dto.AssociadoDto;
import com.sessiomanager.model.Associado;

public class AssociadoMapper {
    public Associado toDocument(AssociadoDto dto){
        return  Associado.builder()
                .id(dto.getId())
                .cpf(dto.getCpf())
                .nome(dto.getNome())
                .build();
    }

    public AssociadoDto toDocumentDto(Associado entity){
        return  AssociadoDto.builder()
                .id(entity.getId())
                .cpf(entity.getCpf())
                .nome(entity.getNome())
                .build();
    }



}

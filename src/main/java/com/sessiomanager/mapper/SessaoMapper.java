package com.sessiomanager.mapper;

import com.sessiomanager.dto.PautaDto;
import com.sessiomanager.dto.SessaoDto;
import com.sessiomanager.model.Pauta;
import com.sessiomanager.model.Sessao;

public class SessaoMapper {
    public Sessao toEntity(SessaoDto dto) {
        return Sessao.builder()
                .id(dto.getId())
                .pauta(Pauta.builder().id(dto.getPauta().getId()).build())
                .time(validarTempoSessao(dto.getTempo()))
                .dataFinalVotacao(dto.getDataFinalVotacao())
                .build();
    }

    private String validarTempoSessao(String time) {
        if(time.equals(""))
            return "60";
        else
            return time;
    }


    public SessaoDto entityToDto(Sessao entity) {
        return SessaoDto.builder()
                .id(entity.getId())
                .tempo(entity.getTime())
                .pauta(PautaDto.builder()
                        .id(entity.getPauta().getId())
                        .titulo(entity.getPauta().getTitulo())
                        .build())
                .dataFinalVotacao(entity.getDataFinalVotacao())
                .build();
    }


}

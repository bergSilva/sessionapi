package com.sessiomanager.endpoints.controller;



import com.sessiomanager.dto.VotoDto;
import com.sessiomanager.dto.custom.VotoResultadoDto;
import com.sessiomanager.endpoints.service.VotoService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/voto")
public class VotoController {

    @Autowired
    private VotoService service;

    @GetMapping
    public List<VotoDto> findAll(){
      return  service.findAll();
    }

    @GetMapping("/{id}")
    public VotoDto findById(@PathVariable Integer id){
        return  service.findById(id);
    }

    @PostMapping
    @ApiOperation("Registro de voto, informar o CPF e o id do associado e ")
    public VotoDto save(@RequestBody VotoDto dto){
        return  service.save(dto);
    }

    @GetMapping("/resultado-votacao")
    @ApiOperation("Relatório sobre o resultado da votação")
    public VotoResultadoDto resultadoVotacao(
            @RequestParam(value = "pautaId") Integer pautaId,
            @RequestParam(value = "sessaoId") Integer sessaoId
            ){
        return  service.resultadoVotacao(pautaId,sessaoId);
    }


}

package com.sessiomanager.endpoints.controller;

import com.sessiomanager.dto.SessaoDto;
import com.sessiomanager.endpoints.service.SessaoService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/sessao")
public class SessaoController {

    @Autowired
    private SessaoService service;

    @GetMapping
    public List<SessaoDto> findAll(){
      return  service.findAll();
    }

    @GetMapping("/{id}")
    public SessaoDto findById(@PathVariable Integer id){
        return  service.findById(id);
    }

    @PostMapping
    @ApiOperation("Abrir uma sessão de votação em uma pauta, parametros tempos da sessao em segundos menor ou igual a 60, identificador da pauta(ID)")
    public SessaoDto save(@RequestBody SessaoDto dto){
        return  service.save(dto);
    }
}

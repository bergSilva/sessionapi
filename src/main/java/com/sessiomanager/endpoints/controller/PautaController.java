package com.sessiomanager.endpoints.controller;

import com.sessiomanager.dto.PautaDto;
import com.sessiomanager.endpoints.service.PautaService;

import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/pauta")
public class PautaController {

    @Autowired
    private PautaService service;

    @GetMapping
    public List<PautaDto> findAll(){
      return  service.findAll();
    }

    @GetMapping("/{id}")
    public PautaDto findById(@PathVariable Integer id){
        return  service.findById(id);
    }

    @PostMapping
    @ApiOperation("Cadastrar uma nova pauta")
    public PautaDto save(@RequestBody PautaDto dto){
        return  service.save(dto);
    }
}

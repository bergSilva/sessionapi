package com.sessiomanager.endpoints.controller;

import com.sessiomanager.dto.AssociadoDto;
import com.sessiomanager.endpoints.service.AssociadoService;
import com.sessiomanager.model.Associado;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/associado")
public class AssociadoController {

    @Autowired
    private AssociadoService service;

    @GetMapping
    public List<AssociadoDto> findAll() {
        return service.findAll();
    }

    @GetMapping("/{id}")
    public AssociadoDto findById(@PathVariable Integer id) {
        return service.findById(id);
    }

    @PostMapping
    public AssociadoDto save(@Valid @RequestBody AssociadoDto dto) {
        return service.save(dto);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable Integer id) {
        service.delete(id);
    }
}

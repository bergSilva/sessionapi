package com.sessiomanager.endpoints.service;

import com.sessiomanager.dto.AssociadoDto;
import com.sessiomanager.model.Associado;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface AssociadoService {

   List<AssociadoDto> findAll();
   AssociadoDto findById(Integer id);
   AssociadoDto save(AssociadoDto associado);
   void delete(Integer id);

}

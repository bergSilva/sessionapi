package com.sessiomanager.endpoints.service.impl;

import com.sessiomanager.dto.SessaoDto;
import com.sessiomanager.endpoints.service.SessaoService;
import com.sessiomanager.exceptions.ResourceNotFoudException;
import com.sessiomanager.mapper.SessaoMapper;
import com.sessiomanager.model.Sessao;
import com.sessiomanager.repository.SessaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.*;

@Service
public class SessaoServiceImpl implements SessaoService {

    @Autowired
    private MessageSource messageSource;

    @Autowired
    private SessaoRepository repository;

    @Override
    public List<SessaoDto> findAll() {
        List<SessaoDto> arrayList = new ArrayList<>();
        repository.findAll().forEach(sessao ->
            arrayList.add(new SessaoMapper().entityToDto(sessao)));
        return arrayList;
    }

    @Override
    public SessaoDto findById(Integer id) {
        Optional<Sessao> optional = repository.findById(id);
        return new SessaoMapper().entityToDto(optional.
                orElseThrow(() ->
                        new ResourceNotFoudException(
                                messageSource.getMessage("sessao.notfoud", null, Locale.getDefault())
                        )));
    }

    @Override
    public SessaoDto save(SessaoDto sessao) {
        Sessao save = new SessaoMapper().toEntity(sessao);
        save.setDataFinalVotacao(dataHoraFinalDaVotacao(sessao));
        return new SessaoMapper().entityToDto(repository.save(save));
    }

    private String dataHoraFinalDaVotacao(SessaoDto sessaoDto) {
        GregorianCalendar gc = new GregorianCalendar();
        SimpleDateFormat sdf2 = new SimpleDateFormat("dd-M-yyyy hh:mm:ss");

        gc.setTimeInMillis(new Date().getTime());

        gc.add(Calendar.SECOND, Integer.parseInt(sessaoDto.getTempo()));

        return sdf2.format(gc.getTime());
    }
}

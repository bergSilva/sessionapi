package com.sessiomanager.endpoints.service.impl;

import com.sessiomanager.dto.PautaDto;
import com.sessiomanager.endpoints.service.PautaService;
import com.sessiomanager.exceptions.ResourceNotFoudException;
import com.sessiomanager.mapper.PautaMapper;
import com.sessiomanager.model.Pauta;
import com.sessiomanager.repository.PautaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Optional;


@Service
public class PautaServiceImpl implements PautaService {

    @Autowired
    private MessageSource messageSource;
    @Autowired
    private PautaRepository repository;

    @Override
    public List<PautaDto> findAll() {
        List<PautaDto> arrayList = new ArrayList<>();
        repository.findAll().forEach(pauta ->
            arrayList.add(new PautaMapper().toDto(pauta))
        );
        return arrayList;
    }

    @Override
    public PautaDto findById(Integer id) {
        Optional<Pauta> pauta = repository.findById(id);
        return new PautaMapper()
                .toDto(pauta.orElseThrow(()
                        -> new ResourceNotFoudException( messageSource.getMessage("pauta.notfoud", null, Locale.getDefault()))));
    }

    @Override
    public PautaDto save(PautaDto pauta) {
        return new PautaMapper().toDto(repository.save(new PautaMapper().toEntity(pauta)));
    }
}

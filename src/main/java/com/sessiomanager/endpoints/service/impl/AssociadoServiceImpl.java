package com.sessiomanager.endpoints.service.impl;

import com.sessiomanager.dto.AssociadoDto;
import com.sessiomanager.endpoints.service.AssociadoService;
import com.sessiomanager.exceptions.ConstraintViolationException;
import com.sessiomanager.exceptions.ResourceNotFoudException;
import com.sessiomanager.mapper.AssociadoMapper;
import com.sessiomanager.model.Associado;
import com.sessiomanager.repository.AssociadoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Optional;

@Service
public class AssociadoServiceImpl implements AssociadoService {


    @Autowired
    private AssociadoRepository repository;
    @Autowired
    private MessageSource messageSource;

    @Override
    public List<AssociadoDto> findAll() {
        List<AssociadoDto> associadoDtos = new ArrayList<>();
        repository.findAll().forEach(associado ->
                associadoDtos.add(new AssociadoMapper().toDocumentDto(associado)));
        return associadoDtos;
    }

    @Override
    public AssociadoDto findById(Integer id) {
        entityExists(id);
        return new AssociadoMapper().toDocumentDto(entityExists(id));
    }

    @Override
    public AssociadoDto save(AssociadoDto associado) {
        return new AssociadoMapper().toDocumentDto(repository.save(new AssociadoMapper().toDocument(associado)));
    }

    @Override
    public void delete(Integer id) {
        try {
            entityExists(id);
            repository.deleteById(id);
        } catch (Exception e) {
            throw new ConstraintViolationException(
                    messageSource.getMessage("constraint.exception", null, Locale.getDefault())
            );
        }

    }

    private Associado entityExists(Integer id) {
        Optional<Associado> optionalAssociado = repository.findById(id);
        return optionalAssociado.orElseThrow(() -> new ResourceNotFoudException(
                messageSource.getMessage("associado.notfoud", null, Locale.getDefault())));
    }
}

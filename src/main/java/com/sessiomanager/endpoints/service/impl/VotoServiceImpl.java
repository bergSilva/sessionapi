package com.sessiomanager.endpoints.service.impl;

import com.sessiomanager.dto.PautaDto;
import com.sessiomanager.dto.ResultadoValidaCpf;
import com.sessiomanager.dto.SessaoDto;
import com.sessiomanager.dto.VotoDto;
import com.sessiomanager.dto.custom.VotoResultadoDto;
import com.sessiomanager.endpoints.service.FeingService;
import com.sessiomanager.endpoints.service.PautaService;
import com.sessiomanager.endpoints.service.SessaoService;
import com.sessiomanager.endpoints.service.VotoService;
import com.sessiomanager.exceptions.DefaultException;
import com.sessiomanager.exceptions.ResourceNotFoudException;
import com.sessiomanager.mapper.SessaoMapper;
import com.sessiomanager.mapper.VotoMapper;
import com.sessiomanager.model.Sessao;
import com.sessiomanager.model.Voto;
import com.sessiomanager.repository.SessaoRepository;
import com.sessiomanager.repository.VotoRepository;
import com.sessiomanager.utils.DateTimes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class VotoServiceImpl implements VotoService {
    @Autowired
    private MessageSource messageSource;

    @Autowired
    private VotoRepository repository;
    @Autowired
    private PautaService pautaService;
    @Autowired
    private SessaoRepository sessaoRepository;
    @Autowired
    private SessaoService sessaoService;
    @Autowired
    private FeingService feingService;

    @Override
    public List<VotoDto> findAll() {
        List<VotoDto> arrayList = new ArrayList<>();
        repository.findAll().forEach(voto ->
                arrayList.add(new VotoMapper().toDocumentDto(voto)));
        return arrayList;
    }

    @Override
    public VotoDto findById(Integer id) {

        return new VotoMapper().toDocumentDto(objectExists(id));
    }

    private Voto objectExists(Integer id) {
        Optional<Voto> votoOptional = repository.findById(id);
        return votoOptional.
                orElseThrow(() ->
                        new ResourceNotFoudException(
                                messageSource.getMessage("voto.notfoud", null, Locale.getDefault())
                        ));
    }

    @Override
    public VotoDto save(VotoDto dto) {
        validarCpf(dto.getAssociado().getCpf());
        validarVotoDoAssociado(dto);
        Sessao sessao = new SessaoMapper().toEntity(sessaoService.findById(dto.getSessao().getId()));
        validaTempoVotacao(sessao);
        return new VotoMapper().toDocumentDto(repository.save(new VotoMapper().toDocument(dto)));
    }

    private void validarCpf(String cpf) {
        ResultadoValidaCpf resultadoValidaCpf = feingService.validaCpf(cpf);
        if (!resultadoValidaCpf.getStatus().equals("ABLE_TO_VOTE"))
            throw new DefaultException(
                    messageSource.getMessage("cpf.inapto", null, Locale.getDefault())
            );
    }

    @Override
    public VotoResultadoDto resultadoVotacao(Integer pautaId, Integer sessaoId) {
        SessaoDto sessaoDto = sessaoService.findById(sessaoId);
        PautaDto pautaDto = pautaService.findById(pautaId);
        Integer votosSim = repository.findByVoto("sim").size();
        Integer votosNao = repository.findByVoto("nao").size();
        Integer totalGeralVotos = votosNao + votosSim;

        return VotoResultadoDto.builder()
                .sessao(String.valueOf(sessaoDto.getId()))
                .tituloPauta(pautaDto.getTitulo())
                .totalGeralDeVotos(totalGeralVotos)
                .totalVotosNao(votosNao)
                .totalVotosSim(votosSim)
                .build();
    }

    public void validaTempoVotacao(Sessao sessao) {
        if (DateTimes.StrTodate(DateTimes.dateToStr(new Date())).getTime() > DateTimes.StrTodate(sessao.getDataFinalVotacao()).getTime())
            throw new DefaultException(
                    messageSource.getMessage("tempo-vonta.esgotado", null, Locale.getDefault())
            );
    }

    private void validarVotoDoAssociado(VotoDto dto) {
        Optional<Voto> optionalVoto = repository.findByAssociadoCpfAndSessaoId(dto.getAssociado().getCpf(), dto.getSessao().getId());
        if (optionalVoto.isPresent())
            throw new DefaultException(
                    messageSource.getMessage("cpf.registrado", null, Locale.getDefault())
            );
    }

}

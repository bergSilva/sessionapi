package com.sessiomanager.endpoints.service;

import com.sessiomanager.dto.PautaDto;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface PautaService {

   List<PautaDto> findAll();
   PautaDto findById(Integer id);
   PautaDto save(PautaDto pauta);

}

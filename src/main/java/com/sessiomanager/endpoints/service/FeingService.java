package com.sessiomanager.endpoints.service;

import com.sessiomanager.dto.ResultadoValidaCpf;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "validaCpf", url = "https://user-info.herokuapp.com")
public interface FeingService {
    @GetMapping(value = "/users/{cpf}", consumes = MediaType.APPLICATION_JSON_VALUE)
    ResultadoValidaCpf validaCpf(@PathVariable String cpf);
}

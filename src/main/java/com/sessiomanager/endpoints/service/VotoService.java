package com.sessiomanager.endpoints.service;

import com.sessiomanager.dto.SessaoDto;
import com.sessiomanager.dto.VotoDto;
import com.sessiomanager.dto.custom.VotoResultadoDto;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Service
public interface VotoService {

   List<VotoDto> findAll();
   VotoDto findById(Integer id);
   VotoDto save(VotoDto voto);
   VotoResultadoDto resultadoVotacao(Integer pautaId, Integer sessaoId);

}

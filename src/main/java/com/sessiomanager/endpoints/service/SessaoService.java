package com.sessiomanager.endpoints.service;

import com.sessiomanager.dto.SessaoDto;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface SessaoService {

   List<SessaoDto> findAll();
   SessaoDto findById(Integer id);
   SessaoDto save(SessaoDto sessao);

}

package com.sessiomanager.model;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;


@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "sessao")
public class Sessao implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "sess_id")
    private Integer id;

    private String time;

    private String dataFinalVotacao;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "paut_id")
    private Pauta pauta;


    @OneToMany(mappedBy = "sessao")
    private List<Voto> voto;
}

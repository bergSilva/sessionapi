package com.sessiomanager.model;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "voto")
public class Voto implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "vot_id")
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "ass_id")
    private Associado associado;

    @ManyToOne
    @JoinColumn(name = "sess_id")
    private Sessao sessao;

    private String voto;

}

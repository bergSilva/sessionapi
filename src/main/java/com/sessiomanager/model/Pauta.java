package com.sessiomanager.model;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;


@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "pauta")
public class Pauta implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "paut_id")
    private Integer id;

    private String titulo;

    @OneToMany(mappedBy = "pauta", fetch = FetchType.EAGER)
    private List<Sessao> sessaoList;

}

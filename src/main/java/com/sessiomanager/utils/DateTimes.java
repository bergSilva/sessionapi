package com.sessiomanager.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateTimes {


    /**
     * Meto de conversão de String para date
     * Dia/Mês/Ano hora:min:seg
     * @param date
     * @return
     */
    public static Date StrTodate(String date){
        SimpleDateFormat format = new SimpleDateFormat("dd-M-yyyy hh:mm:ss");
        try {
            return  format.parse(date);
        } catch (ParseException e) {
            return  null;
        }

    }

    /**
     * Converte Tipe date para String
     * Data Hoha , formato dd-M-yyyy hh:mm:ss
     * @param date
     * @return
     */
    public static String dateToStr( Date date){

        SimpleDateFormat format = new SimpleDateFormat("dd-M-yyyy hh:mm:ss");
        String dateToStr = format.format(date);
        return dateToStr;
    }

}

package com.sessiomanager.controller;

import com.sessiomanager.dto.AssociadoDto;
import com.sessiomanager.endpoints.controller.AssociadoController;
import com.sessiomanager.endpoints.service.AssociadoService;
import com.sessiomanager.model.Associado;
import com.sessiomanager.repository.AssociadoRepository;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.BDDMockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.List;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
public class AssociadoControllerTest {

    @Autowired
    private TestRestTemplate testRestTemplate;

    @MockBean
    private AssociadoController controller;

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void listarTodosOsAssociadosERetornarStatus200(){
      List<AssociadoDto> list = new ArrayList<>();
        list.add(AssociadoDto.builder().id(1).cpf("111111111").nome("Bergson").build());
        list.add(AssociadoDto.builder().id(2).cpf("22222").nome("Bia").build());
        BDDMockito.when(controller.findAll()).thenReturn(list);

        ResponseEntity<String> responseEntity = testRestTemplate.getForEntity("/associado",String.class);
        Assertions.assertThat(responseEntity.getStatusCodeValue()).isEqualTo(200);
    }


    @Test
    public void pequisarUmAssociadosPeloIdERetornarStatus200(){
        AssociadoDto dto =AssociadoDto.builder().id(1).cpf("22222").nome("Bia").build();
        BDDMockito.when(controller.findById(1)).thenReturn(dto);
        ResponseEntity<AssociadoDto> responseEntity = testRestTemplate.getForEntity("/associado/{id}",AssociadoDto.class,dto.getId());
        Assertions.assertThat(responseEntity.getStatusCodeValue()).isEqualTo(200);
    }

    @Test
    public void pequisarUmAssociadosPeloIdERetornarStatus404(){
        ResponseEntity<AssociadoDto> responseEntity = testRestTemplate.getForEntity("/{id}",AssociadoDto.class,-100);
        Assertions.assertThat(responseEntity.getStatusCodeValue()).isEqualTo(404);
    }

}

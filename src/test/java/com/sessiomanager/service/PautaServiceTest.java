package com.sessiomanager.service;

import com.sessiomanager.dto.PautaDto;
import com.sessiomanager.endpoints.service.impl.PautaServiceImpl;
import com.sessiomanager.mapper.PautaMapper;
import com.sessiomanager.model.Pauta;
import com.sessiomanager.repository.PautaRepository;
import org.hibernate.validator.constraints.br.TituloEleitoral;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(SpringJUnit4ClassRunner.class)
public class PautaServiceTest {

    @Mock
    private PautaRepository pautaRepository;

    @InjectMocks
    private PautaServiceImpl pautaService;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }


    @Test
    public void listarTotasAsPautasERetonarOTamanhoDaLista() {
        List<Pauta> pautas = new ArrayList<>();
        pautas.add(Pauta.builder().id(1).titulo("Nova Pauta").build());
        pautas.add(Pauta.builder().id(2).titulo("Velha Pauta").build());
        pautas.add(Pauta.builder().id(3).titulo("Media Pauta").build());
        pautas.add(Pauta.builder().id(4).titulo("Quase Velha Pauta").build());
        when(pautaRepository.findAll()).thenReturn(pautas);
        List<PautaDto> pautaDtos = pautaService.findAll();
        assertEquals(4, pautaDtos.size());
    }

    @Test
    public void  pesquisarPautaPorIdEVerificarDadosRetornados(){
        Pauta pauta = Pauta.builder().id(1).titulo("Nova Pauta").build();
        BDDMockito.when(pautaRepository.getOne(1)).thenReturn(pauta);
        PautaDto result = pautaService.findById(1);
        assertEquals(1, result.getId());
        assertEquals("Nova Pauta", result.getTitulo());
    }

    @Test
    public void salvarPauta(){
        PautaDto pauta = PautaDto.builder().id(1).titulo("Delly novo horario").build();
        when(pautaRepository.save(new PautaMapper().toEntity(pauta))).thenReturn(new PautaMapper().toEntity(pauta));
        PautaDto result = pautaService.save(pauta);
        assertEquals(1, result.getId());
        assertEquals("Delly novo horario", result.getTitulo());
    }
}
